
const express = require('express')
const fs = require('fs')
const https = require('https')
const path = require('path')
const axios = require('axios')
const app = express()

const {call} = require('./call')

require('dotenv').config()

app.use(express.json())

app.post('/api', call)

app.listen(5000, () => console.log('Server running on port 5000'))