#!/usr/bin/env bash

set -e
echo "This script will detect Java 8 or Java 11 based on the current JAVA_HOME and import the T-Mobile root certs"

version_output=$(java -version 2>&1 | grep version)
echo "Detected Java Version: $version_output"
echo "JAVA_HOME: $JAVA_HOME"




if [[ -z $JAVA_HOME ]]
then
    echo 'JAVA_HOME not set'
    exit 1
fi

STORE_PASS=$1
: ${STORE_PASS:="changeit"}


CERT_PATHS=( "$JAVA_HOME/jre/lib/security" "$JAVA_HOME/lib/security"  )

for CERT_PATH in "${CERT_PATHS[@]}"
do
  if [ -d "$CERT_PATH" ]; then
    # copy the cacerts to jssecacerts
    KEYSTORE="${CERT_PATH}/jssecacerts"
    cp "${CERT_PATH}/cacerts" "$KEYSTORE"

    echo "Adding cdp artifactory certificate"
    keytool -printcert -sslserver artifactory.cdp.t-mobile.com -rfc | keytool -import -noprompt -alias tmocerts-cdp -keystore "$KEYSTORE" -storepass "$STORE_PASS"

    echo "Adding edp artifactory certificate"
    keytool -printcert -sslserver artifactory.service.edp.t-mobile.com -rfc | keytool -import -noprompt -alias tmocerts-edp -keystore "$KEYSTORE" -storepass "$STORE_PASS"

    echo "Adding bintray certificate"
    keytool -printcert -sslserver bintray.com -rfc | keytool -import -noprompt -alias tmocerts-bin  -keystore "$KEYSTORE" -storepass "$STORE_PASS"

    echo "Adding oss sonotype certificate"
    keytool -printcert -sslserver oss.sonatype.org -rfc | keytool -import -noprompt -alias sonatype -keystore "$KEYSTORE" -storepass "$STORE_PASS"
    
    echo "Adding repo.maven.apache.org certificate"
    keytool -printcert -sslserver repo.maven.apache.org -rfc | keytool -import -noprompt -alias maven-central-repo -keystore "$KEYSTORE" -storepass "$STORE_PASS"

    echo "Adding repo1.maven.org certificate"
    keytool -printcert -sslserver repo1.maven.org -rfc | keytool -import -noprompt -alias maven-central-repo1 -keystore "$KEYSTORE" -storepass "$STORE_PASS"
    
    echo "Adding nodejs.org certificate"
    keytool -printcert -sslserver nodejs.org -rfc | keytool -import -noprompt -alias nodejs -keystore "$KEYSTORE" -storepass "$STORE_PASS"
    
    echo "Adding gitlab.com certificate"
    keytool -printcert -sslserver gitlab.com -rfc | keytool -import -noprompt -alias gitlab-com -keystore "$KEYSTORE" -storepass "$STORE_PASS"

    echo "Adding TMO Enterprise Root certificates"
    echo "----------"
    keytool -noprompt -importcert -file "./certs/T-Mobile Enterprise Root CA 2.crt" -alias tmocerts-enterprise-root -keystore "$KEYSTORE" -storepass "$STORE_PASS"
    keytool -noprompt -importcert -file "./certs/T-Mobile USA Enterprise Root CA.crt" -alias tmocerts-usa-enterprise-root -keystore "$KEYSTORE" -storepass "$STORE_PASS"
    keytool -noprompt -importcert -file "./certs/T-Mobile USA Root CA.crt" -alias tmocerts-usa-root -keystore "$KEYSTORE" -storepass "$STORE_PASS"

    keytool -list -keystore "$KEYSTORE" -storepass "$STORE_PASS"
  
  fi
done

echo "DONE! You may need to restart running java processes for these changes to take effect."

