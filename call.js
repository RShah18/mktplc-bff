// process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // 1. Uncomment to bypass ssl verification
const fs = require('fs')
const https = require('https')
const path = require('path')
const axios = require('axios')

const rootCertPath = './certs/cert-not-meg.pem' // copied the below installed cert in a separate file
// installed cert path on Ubuntu --> '/etc/ssl/certs/certificate-tmo-ssl.pem'

const call = async (req, res) => {
  const CA = [fs.readFileSync(rootCertPath)];
  const agent = new https.Agent({
    // rejectUnauthorized: false, // 2. Uncomment to bypass ssl verification
    ca: CA // 3. Attach CA certified Root certificate
  });
  const { headers, body } = req;
  const url = 'https://dmt-pal-consumer-onboarding-qlab01.geo-npe.mesh.t-mobile.com/dmt-pal/v1/consumer-onboarding/consumers';
  console.log('Trying')
  try {
    const resp = await axios({
      url,
      method: 'post',
      httpsAgent: agent,
      headers,
      data: body
    });
    console.log('res', resp);
    res.send(resp);
  } catch(e) {
    console.log(e)
    console.error(e.response)
    res.send(`${e}`);
  }
}

module.exports = { call }